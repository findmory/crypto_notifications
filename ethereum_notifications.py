import requests
import time
from datetime import datetime
import settings

ETH_API_URL = 'https://api.coinmarketcap.com/v1/ticker/ethereum'
ETH_PRICE_THRESHOLD = 600


def build_ifttt_url(event):
    return 'https://maker.ifttt.com/trigger/{}/with/key/{}'.format(event, settings.IFTT_KEY)


def get_latest_ethereum_price():
    response = requests.get(ETH_API_URL)
    response.json = response.json()
    # Convert the price to a float
    return float(response.json[0]['price_usd'])


def post_ifttt_webhook(event, value):
    # The payload that will be sent to IFTTT service
    data = {'value1': value}

    # inserts our desired event
    ifttt_event_url = build_ifttt_url(event)

    # send HTTP POST to the webhook URL
    requests.post(ifttt_event_url, json=data)


def format_eth_history(eth_history):
    rows = []
    for eth_price in eth_history:
        # Formats the date into a string: '02.09.2018 15:09'
        date = eth_price['date'].strftime('%m.%d.%Y %H:%M')
        price = eth_price['price']
        # <b> (bold) tag creates bolded text
        # 24.02.2018 15:09: $<b>10123.4</b>
        row = '{}: $<b>{}</b>'.format(date, price)
        rows.append(row)

    # Use a <br> (break) tag to create a new line
    # Join the rows delimited by <br> tag: row1<br>row2<br>row3
    return '<br>'.join(rows)


def main():
    eth_history = []
    while True:
        print('getting latest price')
        price = get_latest_ethereum_price()
        date = datetime.now()
        eth_history.append({'date': date, 'price': price})

        # Send emergency notification
        if price < ETH_PRICE_THRESHOLD:
            print('sending emergency update')
            post_ifttt_webhook('eth_price_emergency', price)

        # Send Messenger notification
        # once we have 5 items in our eth_history
        if len(eth_history) == 5:
            print('sending update to messenger')
            post_ifttt_webhook('eth_price_update', format_eth_history(eth_history))

            # reset history
            eth_history = []

        # Sleep for 5 minutes before making next request
        time.sleep(5 * 60)


if __name__ == '__main__':
    main()
